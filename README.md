# CSCI 4588 - Machine Learning II - Programming Assignment #1
Author: Nolan Sherman  
Professor: Dr. T. Hoque

This project conains the source files and compiled executables for 
programming assignment #1.

# Executing Included Binaries
Nothing extra needs to be installed to run any of the following binaries built with GO (part 2 and 3). You
do not need to install GOLang or anyother tools, nor run the scripts mentioned above.
## Part1
Part 1 however does require octave to be installed. To execute the solution please follow the
following instruction after installing Octave.
1. start Octave CLI
2. ```cd``` into assignemnet-1/part1
3. Run ```run solution_part1.m``` 
4. The solution will be printed to terminal and outputed to Output_MSE.txt

Please Note:
- solution_part1.m is the source code
- The output for this solution has already been generated to Output_MSE.txt
- The answer is technichally the root of the squared error.

## Part2
1. From terminal, ```cd``` into assignment-1/part2
2. Run ```./part2_solution.exe --exitFile EXIT_CONDITIONs.json```
3. May take about a minute to terminate. Optionaly pipe output to a file.

Please Note: 
- The output of this solution has already been printed to Output_HillClimbing.txt
- The source code is located in gopath/src/assignment/part2
- The answer is technichally the root of the squared error.


## Part3
1. From terminal, ```cd``` into assignment-1/part3
2. Run ```./part3_solution.exe --params params.json```
3. Optionaly pipe output to a file.

Please Note: 
- The output of this solution has already been printed to Output_SGA.txt
- The source code is located in gopath/src/assignment/part3

# Building Go executables
Before debugging, building, or running GO source code you must run the following:
1. Run `source ./environment.sh` to set up the environment variables pertaining to GOPATH etc.
2. Run `./goinstall.sh` so that the required go libraries will be installed locally to gopath
3. See Golang.org to learn about how to run, debug and build source code.

