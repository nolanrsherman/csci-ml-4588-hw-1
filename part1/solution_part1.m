# Author: Nolan Sherman
# CSCI 4588 - Programming Assignment 1 - Part 1

#Load housing data
load ../HousingData/X.txt;
load ../HousingData/Y.txt;

#Calculated betas
b = inv(X' * X) * X' * Y;
#size
[N,M]=size(X);
#Calculate Squared Error
E = (Y-(X*b))'*(Y-(X*b));
#Calculate MSE
MSE=sqrt(E/N)

#Write MSE to file
filename = "Output_MSE.txt";
fid = fopen(filename, "w");
fputs(fid, "MSE:");
fdisp(fid, MSE);
fclose (fid);