# Fall 2019: CSCI 4/5588 Programming Assignment #1
Author: Nolan Sherman  
ID: 2397895  

# Part 1
Source :
```c
// Author: Nolan Sherman
// CSCI 4588 - Programming Assignment 1 - Part 1

//Load housing data
load ../HousingData/X.txt;
load ../HousingData/Y.txt;

//Calculated betas
b = inv(X' * X) * X' * Y;
//size
[N,M]=size(X);
//Calculate Squared Error
E = (Y-(X*b))`*(Y-(X*b));
//Calculate MSE
MSE=sqrt(E/N)

//Write MSE to file
filename = "Output_MSE.txt";
fid = fopen(filename, "w");
fputs(fid, "MSE:");
fdisp(fid, MSE);
fclose (fid);
```
Output : ```MSE: 82658.73751```


# Part 2

Source :
```go
// Author: Nolan Sherman - nolanrsherman@gmail.com

//Package part2/main is an executable package that find the betas
//with the lowest MSE for an input Xi and Ouput Y.
package main

import (
	"assignment/bits"
	"assignment/input"
	"assignment/regression"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	mathbits "math/bits"
	"math/rand"

	"gonum.org/v1/gonum/mat"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	exitFile := initFlags()
	params := parseExitFile(exitFile)
	X, err := input.LoadMatrix(params.XFile) //X input
	Y, err := input.LoadMatrix(params.YFile) //Y output
	check(err)
	_, numBetas := X.Dims()
	betaRange := int64(params.BetaRange)
	maxBits := maxBits(betaRange)

	GlobalBetas := genRandomBits(betaRange, numBetas)
	//var finalBetas *[]bits.BitArray
	for i := 0; i < params.MaxRestart; i++ {

		localBetas := genRandomBits(betaRange, numBetas) //create random betas
		foundLocalBest := false
		j := 0
		for (j < params.MaxGeneration) && !foundLocalBest {
			j = j + 1
			var bestBetas *[]bits.BitArray
			//Generate Neighbors
			for k := 0; k < maxBits; k++ {
				for l := 0; l < numBetas; l++ {
					neighbor := genNeighbor(localBetas, l, k)
					bestBetas = compareNeighbors(betaRange, X, Y, localBetas, neighbor)
				}
			}

			if bestBetas == localBetas {
				foundLocalBest = true
			}
		}

		GlobalBetas = compareNeighbors(betaRange, X, Y, GlobalBetas, localBetas)
		Lb := bitsToMatrix(*localBetas, float64(betaRange)) //local beta
		LMse := regression.MSE(X, Y, Lb)
		printMatrix(fmt.Sprintf("#%d : mse:%v", i, LMse), Lb)
	}

	Gb := bitsToMatrix(*GlobalBetas, float64(betaRange)) //GlobalBetas
	GMse := regression.MSE(X, Y, Gb)
	printMatrix(fmt.Sprintf("#Best Betas Found : mse:%v", GMse), Gb)
}

func printMatrix(label string, m *mat.Dense) {
	x := m.T()
	formatted := mat.Formatted(x, mat.Prefix(""), mat.Squeeze())
	fmt.Printf(label+" = %v\n", formatted)
}

//compareNeighbors returns the *BitArray of Betas with the best MSE
func compareNeighbors(betaRange int64, X, Y *mat.Dense, first, second *[]bits.BitArray) *[]bits.BitArray {

	b1, b2 := bitsToMatrix(*first, float64(betaRange)), bitsToMatrix(*second, float64(betaRange))
	firstMSE, secondMSE := regression.MSE(X, Y, b1), regression.MSE(X, Y, b2)

	if firstMSE > secondMSE {
		return second
	}
	return first
}

func genNeighbor(bitarrays *[]bits.BitArray, beta int, bit int) *[]bits.BitArray {
	var neighbor = append([]bits.BitArray(nil), *bitarrays...) //copy the given bitarrays.
	neighbor[beta] = neighbor[beta].Flip(uint(bit))
	return &neighbor
}

func genRandomBits(betaRange int64, numBetas int) *[]bits.BitArray {
	var bitarrays = make([]bits.BitArray, numBetas)
	maxBits := maxBits(betaRange)
	for i := 0; i < numBetas; i++ {
		bits := bits.NewBitArray64(maxBits, uint64(rand.Int63n(betaRange)))
		bitarrays[i] = bits
	}
	return &bitarrays
}

func maxBits(betaRange int64) int {
	return mathbits.Len64(uint64(betaRange))
}

func fitness(X, Y, b *mat.Dense) float64 {
	return regression.MSE(X, Y, b)
}

func bitsToMatrix(bits []bits.BitArray, betaRange float64) *mat.Dense {
	var betas = make([]float64, len(bits))
	for i, data := range bits {
		num := float64(data.Value().(uint64))
		betas[i] = num - (betaRange / 2)
	}
	return mat.NewDense(len(bits), 1, betas)
}
```
Output : 
```txt
(...)
#84 : mse:4.610495009128303e+07 = [-4826  -42117  -7247  -21451]
#85 : mse:9.557771869357373e+07 = [22750  -39288  9233  -44695]
#86 : mse:9.939738706678116e+07 = [14679  45364  12398  -46611]
#87 : mse:2.041993562646602e+07 = [8496  8739  -15563  9687]
#88 : mse:1.5722582178467587e+07 = [-46692  -18479  24175  7500]
#89 : mse:1.6846755966806002e+07 = [23079  -30789  -32522  -7724]
#90 : mse:7.087755341254145e+07 = [12199  -30768  -6866  33404]
#91 : mse:1.4168026022566563e+07 = [-7188  -14110  5040  6770]
#92 : mse:2.5904264597160973e+07 = [12326  -2064  34446  -12089]
#93 : mse:4.561789900624912e+07 = [-529  31513  31173  -21377]
#94 : mse:9.614155572112665e+07 = [37947  -42275  -27983  -44918]
#95 : mse:3.6151304469165266e+07 = [20903  24987  4058  17015]
#96 : mse:1.0550879874413053e+07 = [7904  31105  -17629  5027]
#97 : mse:5.286326434802147e+07 = [26516  11517  25031  -24751]
#98 : mse:6.42548288622371e+07 = [-28983  -23192  9764  30284]
#99 : mse:5.179501690350341e+07 = [27471  18438  15729  24347]
#Best Betas Found : mse:85121.30991485035 = [13850  -28621  16812  113]
```

# Part 3

Source :

f.go
```go
// Author: Nolan Sherman - nolanrsherman@gmail.com
package main

import "math"

//F is the function this program attempts to optimize
var F = func(betas [10]float64) float64 {

	//Calculating Part A
	a := float64(1) // 1

	//Calculating part B
	b := float64(0)
	for i := 0; i < len(betas); i++ {
		x := betas[i]
		b = b + (x * x / 4000)
	}

	//Calculating part C
	cx := func(i int, x float64) float64 {
		return math.Cos(x / math.Sqrt(float64(i)))
	}

	c := cx(1, betas[0])
	for i := 2; i < len(betas); i++ {
		c = c * cx(i, betas[i-1])
	}

	//Return the value for function
	return a + b - c

}

```

gene.go
```go
// Author: Nolan Sherman - nolanrsherman@gmail.com

package main

import (
	"assignment/bits"
	"math/rand"
)

const maxCrossover = 50

//Gene16 represents a gene for our Genetic Algorithm
type Gene16 struct {
	Bits    []bits.BitArray
	Length  int
	MaxBits int
	Fitness float64
}

//NewGene16 returns a new gene that uses 16bit variable blocks.
//Recieves: maxBits int => The max number of bits to use of the 16 available.
//length => How many blocks this Gene has.
// values ... => list of values for the gene.
func NewGene16(maxBits, length int, values ...uint16) Gene16 {
	bitarr := make([]bits.BitArray, 0, length)
	gene := Gene16{bitarr, length, maxBits, 0}
	if len(values) > length {
		panic("Slice of values passed to NewGene16() has greater length than given value.")
	}
	for _, v := range values {
		bits := bits.NewBitArray16(maxBits, v)
		gene.Bits = append(gene.Bits, bits)
	}

	return gene
}

//ValuesFloat64 returns the list of betas this gene
//represents in a slice []float64
func (gene *Gene16) ValuesFloat64() []float64 {
	data := gene.Bits
	values := make([]float64, 0, gene.Length)
	offset := int16(1 << uint(gene.MaxBits-1))
	for _, bits := range data {
		v := int16(bits.Value().(uint16)) - offset
		values = append(values, float64(v))
	}

	return values
}

//Replicate returns an exact deep copy of this Gene, not a reference
//to original underlying data structures.
func (gene *Gene16) Replicate() Gene16 {
	var bits = append([]bits.BitArray(nil), gene.Bits...) //copy the given bitarrays.
	return Gene16{bits, gene.Length, gene.MaxBits, gene.Fitness}
}

//Cross performs as random genetic cross operation between
//this gene and a given mate. It returns the two resulting
//genes.
func (gene *Gene16) Cross(mate *Gene16) (Gene16, Gene16) {

	setA := gene.Replicate()
	setB := mate.Replicate()
	pA := rand.Intn(len(setA.Bits)) //positionA
	pB := rand.Intn(len(setB.Bits)) //positionB
	A := setA.Bits[pA]
	setA.Bits[pA] = setB.Bits[pB]
	setB.Bits[pB] = A

	return setA, setB
}
```

population.go
```go
// Author: Nolan Sherman - nolanrsherman@gmail.com
package main

import (
	"math/rand"
)

//Population is a collection of Genes
//implements sort.interface
type Population struct {
	genes   []Gene16
	fitness func(*Gene16) float64
}

//NewPopulation returns a new poulation with the given genes.
func NewPopulation(genes []Gene16, fitness func(*Gene16) float64) *Population {
	pop := &Population{genes, fitness}
	pop.calcFitness()
	return pop
}

//calcFitness calculates the fitness of each gene in
//the population with the given fitness function
func (pop *Population) calcFitness() {
	for i := 0; i < len(pop.genes); i++ {
		pop.genes[i].Fitness = pop.fitness(&pop.genes[i])
	}
}

// Len is the number of elements in the collection.
func (pop *Population) Len() int {
	return len(pop.genes)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (pop *Population) Less(i, j int) bool {
	return pop.genes[i].Fitness > pop.genes[j].Fitness
}

// Swap swaps the elements with indexes i and j.
func (pop *Population) Swap(i, j int) {
	a := pop.genes[i]
	pop.genes[i] = pop.genes[j]
	pop.genes[j] = a
}

//CloneAll returns the reference to a deep copy of
//the original population
func (pop *Population) CloneAll() *Population {
	return pop.cloneRange(0, pop.Len())
}

//Harvest returns two []Gene based on the specified ratios. Assumes Population
//has already been sorted based on fitness.
func (pop *Population) Harvest(eliteRatio float64) (*Population, *Population) {
	numElite := int(eliteRatio * float64(pop.Len()))
	elite := pop.cloneRange(0, numElite)
	cross := pop.cloneRange(0, pop.Len())
	return elite, cross
}

//cloneRange returns a deep copy of the population from a given range
func (pop *Population) cloneRange(start, end int) *Population {
	genes := make([]Gene16, 0, end-start)
	for _, gene := range pop.genes[start:end] {
		genes = append(genes, gene.Replicate())
	}
	return &Population{genes, pop.fitness}
}

//Add adds the given populations to this population.
func (pop *Population) Add(populations ...*Population) {
	for _, newPop := range populations {
		pop.genes = append(pop.genes, newPop.genes...)
	}
}

//CrossOver runs a cross over algorithm on a given
//ratio of this population. Assumes population is sorted.
//The current state of this population is overwritten.
func (pop *Population) CrossOver(ratio float64) {
	numPairs := int(ratio * float64(pop.Len()) / 2)

	genes := make([]Gene16, 0, numPairs*2)
	sumFitness := pop.sumFitness()
	for i := 0; i < numPairs; i++ {
		a := pop.rouletteSelect(sumFitness)
		b := pop.rouletteSelect(sumFitness)

		//perform cross
		ab, ba := a.Cross(&b)
		genes = append(genes, ab, ba)
	}
	pop.genes = genes //switchout for the new genes
	pop.calcFitness() //re calculate fitness
}

func (pop *Population) sumFitness() float64 {
	var sum float64
	for _, gene := range pop.genes {
		sum = sum + gene.Fitness
	}
	return sum
}

//rouletteSelect selects a gene using roulette algorithm. Assumes population
//is sorted by fitness.
func (pop *Population) rouletteSelect(fitnessTotal float64) Gene16 {
	r := rand.Float64()
	pF := float64(1)
	for _, gene := range pop.genes {
		p := gene.Fitness / fitnessTotal //probability of acceptance
		offset := pF - p
		if r < pF && r > offset {
			return gene
		}
		pF = offset
	}
	panic("rouletteSelect could not make a selection.")
}

//Mutate randomly mutates genes in this population
//where the number of genes is determined by mutationRate
func (pop *Population) Mutate(mutationRate float64) {
	num := int(float64(pop.Len()) * mutationRate)
	for i := 0; i < num; i++ {
		j := rand.Intn(pop.Len())
		l := rand.Intn(len(pop.genes[0].Bits))
		b := rand.Intn(pop.genes[l].MaxBits)
		pop.genes[j].Bits[l].Flip(uint(b))
	}
}

```

main.go
```go

// Author: Nolan Sherman - nolanrsherman@gmail.com

//Package main is an executable package that computates the betas
//to maximize F (defined in f.go) non-deterministicly using
//a Simple Genetic Algorithm
package main

import (
	"fmt"
	"math/rand"
	"sort"
	"strings"
)

//NumBetas is the number of betas (variables) we are using
const NumBetas = 10

//MaxBitLength specifies the bit range to use in variables
//this will limit our betas to -1024 < x < 1023
const MaxBitLength = 11

func main() {
	params := initParams() //initializes params from passed json file.

	iterations := make([][]*Population, 0, params.MaxIterations)
	for i := 0; i < params.MaxIterations; i++ {
		generations := make([]*Population, 0, params.MaxGenerations)
		population := initialze(params.PopulationSize)
		sort.Sort(population)
		generations = append(generations, population)

		for j := 0; j < params.MaxGenerations; j++ {
			generation := evolve(population, params.EliteRate, params.CrossoverRate, params.MutationRate, params.PopulationSize)
			sort.Sort(generation)
			generations = append(generations, generation)
			population = generation
		}
		iterations = append(iterations, generations)
	}

	printResult(iterations)
}

//printResult prints the results given a [][]*Population
func printResult(iterations [][]*Population) {
	var sb strings.Builder
	t1 := "\n\n------ Iteration %v --------"
	t2 := "\n# Generation = %v, Fitness = %v"
	t3 := "\n////////////////////////////"
	for i, generations := range iterations {
		sb.WriteString(fmt.Sprintf(t1, i))
		for j, population := range generations {
			fitness := population.genes[0].Fitness
			sb.WriteString(fmt.Sprintf(t2, j, fitness))
		}
		sb.WriteString(t3)
	}
	fmt.Println(sb.String())
}

func evolve(population *Population, eliteRate, crossoverRate, mutationRate float64, nextSize int) *Population {
	elite, other := population.Harvest(eliteRate)
	other.CrossOver(crossoverRate)
	other.Mutate(mutationRate)
	randgenes := randomGenes(nextSize - elite.Len() - other.Len())
	newPop := NewPopulation(randgenes, fitness)
	newPop.Add(elite, other)
	return newPop
}

//initialze creates the initial population of Genes
//Recieves: size int => The size of the population
func initialze(size int) *Population {
	genes := randomGenes(size)
	return NewPopulation(genes, fitness)
}

//randomGenes creates a list of randomly generated genes
//of the given size.
func randomGenes(size int) []Gene16 {
	population := make([]Gene16, 0, size)
	maxInt := (1 << MaxBitLength) - 1 //the biggest int that can be made with bit length of MaxBitLength
	for i := 0; i < size; i++ {
		gene := NewGene16(MaxBitLength, NumBetas, randomBetas(NumBetas, maxInt)...)
		population = append(population, gene)
	}
	return population
}

//randomBetas returns a slice of int32 for random integers
//with given length and a maximuum value of max
func randomBetas(size, max int) []uint16 {
	values := make([]uint16, 0, size)
	for i := 0; i < size; i++ {
		rand := rand.Int31n(int32(max))
		values = append(values, uint16(rand))
	}
	return values
}

//fitness computes the fitness of a given
//gene in relation to F()
func fitness(gene *Gene16) float64 {
	rawBetas := gene.ValuesFloat64()
	var betas [10]float64
	copy(betas[:], rawBetas[:10])
	return F(betas)
}
```

Output:
```txt
(...)
------ Iteration 4 --------
# Generation = 0, Fitness = 1587.7694537532589
# Generation = 1, Fitness = 1658.4095367780506
# Generation = 2, Fitness = 1658.4095367780506
# Generation = 3, Fitness = 1796.4660465238917
# Generation = 4, Fitness = 1821.0009357324932
# Generation = 5, Fitness = 1821.0009357324932
# Generation = 6, Fitness = 2005.8175776386643
# Generation = 7, Fitness = 2007.5015776386642
# Generation = 8, Fitness = 2159.5837857717647
# Generation = 9, Fitness = 2159.5837857717647
# Generation = 10, Fitness = 2167.22842116792
# Generation = 11, Fitness = 2192.840070969164
# Generation = 12, Fitness = 2195.3555647204107
# Generation = 13, Fitness = 2388.0131711679196
# Generation = 14, Fitness = 2388.0131711679196
# Generation = 15, Fitness = 2392.5810886548443
# Generation = 16, Fitness = 2444.272701543377
# Generation = 17, Fitness = 2444.272701543377
# Generation = 18, Fitness = 2458.8631061413275
# Generation = 19, Fitness = 2458.8631061413275
# Generation = 20, Fitness = 2458.8631061413275
# Generation = 21, Fitness = 2468.3096472630464
# Generation = 22, Fitness = 2484.6028016248692
# Generation = 23, Fitness = 2484.6028016248692
# Generation = 24, Fitness = 2492.5555001231446
# Generation = 25, Fitness = 2492.5555001231446
# Generation = 26, Fitness = 2492.5555001231446
# Generation = 27, Fitness = 2496.039735351242
# Generation = 28, Fitness = 2500.2444385138406
# Generation = 29, Fitness = 2505.2852136954625
# Generation = 30, Fitness = 2509.271558199423
# Generation = 31, Fitness = 2520.1905527600716
# Generation = 32, Fitness = 2520.6868027600713
# Generation = 33, Fitness = 2520.6868027600713
# Generation = 34, Fitness = 2529.1906914922756
# Generation = 35, Fitness = 2529.1906914922756
# Generation = 36, Fitness = 2529.1906914922756
# Generation = 37, Fitness = 2536.1502246176233
# Generation = 38, Fitness = 2537.6992121185467
# Generation = 39, Fitness = 2537.6992121185467
# Generation = 40, Fitness = 2537.6992121185467
# Generation = 41, Fitness = 2545.3226518964348
# Generation = 42, Fitness = 2545.3226518964348
# Generation = 43, Fitness = 2546.2034621185467
# Generation = 44, Fitness = 2546.2034621185467
# Generation = 45, Fitness = 2553.827212118547
# Generation = 46, Fitness = 2553.827212118547
# Generation = 47, Fitness = 2553.827212118547
# Generation = 48, Fitness = 2553.827212118547
# Generation = 49, Fitness = 2553.827212118547
# Generation = 50, Fitness = 2558.9384381592986
(...)
```