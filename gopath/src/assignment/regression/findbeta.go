//Author: Nolan Sherman

package regression

import (
	"math"

	"gonum.org/v1/gonum/mat"
)

// FindBeta takes in a matrices X and Y and produces the betas
// for the linear regression. Matlab equivelent of inv(X'*X)*X'*Y
func FindBeta(X *mat.Dense, Y *mat.Dense) *mat.Dense {

	//transpose X
	tX := X.T()
	//printMatrix("X'", &tX)

	//inverse(X'X)
	var inv mat.Dense
	inv.Mul(tX, X)
	inv.Inverse(&inv)

	//X'Y
	var tXy mat.Dense
	tXy.Mul(tX, Y)

	//betas
	var b mat.Dense
	b.Mul(&inv, &tXy)
	return &b
}

// MSE returns the MSE for a given X, Y and set of appropriately size Betas.
func MSE(X *mat.Dense, Y *mat.Dense, betas *mat.Dense) float64 {
	// Num of points
	N, _ := X.Dims()

	// #Calculate Squared Error
	// E = (Y-(X*b))'*(Y-(X*b))
	var Xb mat.Dense //(X*b)
	Xb.Mul(X, betas)

	var YsubXb mat.Dense // (Y-(X*b))
	YsubXb.Sub(Y, &Xb)

	var E mat.Dense
	E.MulElem(&YsubXb, &YsubXb)

	// #Calculate MSE
	// MSE=E/N
	MSE := math.Sqrt(mat.Sum(&E) / float64(N))

	return MSE
}
