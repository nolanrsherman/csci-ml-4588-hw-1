// Author: Nolan Sherman - nolanrsherman@gmail.com

//Package part2/main is an executable package that find the betas
//with the lowest MSE for an input Xi and Ouput Y.
package main

import (
	"assignment/bits"
	"assignment/input"
	"assignment/regression"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	mathbits "math/bits"
	"math/rand"

	"gonum.org/v1/gonum/mat"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	exitFile := initFlags()
	params := parseExitFile(exitFile)
	X, err := input.LoadMatrix(params.XFile) //X input
	Y, err := input.LoadMatrix(params.YFile) //Y output
	check(err)
	_, numBetas := X.Dims()
	betaRange := int64(params.BetaRange)
	maxBits := maxBits(betaRange)

	GlobalBetas := genRandomBits(betaRange, numBetas)
	//var finalBetas *[]bits.BitArray
	for i := 0; i < params.MaxRestart; i++ {

		localBetas := genRandomBits(betaRange, numBetas) //create random betas
		foundLocalBest := false
		j := 0
		for (j < params.MaxGeneration) && !foundLocalBest {
			j = j + 1
			var bestBetas *[]bits.BitArray
			//Generate Neighbors
			for k := 0; k < maxBits; k++ {
				for l := 0; l < numBetas; l++ {
					neighbor := genNeighbor(localBetas, l, k)
					bestBetas = compareNeighbors(betaRange, X, Y, localBetas, neighbor)
				}
			}

			if bestBetas == localBetas {
				foundLocalBest = true
			}
		}

		GlobalBetas = compareNeighbors(betaRange, X, Y, GlobalBetas, localBetas)
		Lb := bitsToMatrix(*localBetas, float64(betaRange)) //local beta
		LMse := regression.MSE(X, Y, Lb)
		printMatrix(fmt.Sprintf("#%d : mse:%v", i, LMse), Lb)
	}

	Gb := bitsToMatrix(*GlobalBetas, float64(betaRange)) //GlobalBetas
	GMse := regression.MSE(X, Y, Gb)
	printMatrix(fmt.Sprintf("#Best Betas Found : mse:%v", GMse), Gb)
}

func printMatrix(label string, m *mat.Dense) {
	x := m.T()
	formatted := mat.Formatted(x, mat.Prefix(""), mat.Squeeze())
	fmt.Printf(label+" = %v\n", formatted)
}

//compareNeighbors returns the *BitArray of Betas with the best MSE
func compareNeighbors(betaRange int64, X, Y *mat.Dense, first, second *[]bits.BitArray) *[]bits.BitArray {

	b1, b2 := bitsToMatrix(*first, float64(betaRange)), bitsToMatrix(*second, float64(betaRange))
	firstMSE, secondMSE := regression.MSE(X, Y, b1), regression.MSE(X, Y, b2)

	if firstMSE > secondMSE {
		return second
	}
	return first
}

func genNeighbor(bitarrays *[]bits.BitArray, beta int, bit int) *[]bits.BitArray {
	var neighbor = append([]bits.BitArray(nil), *bitarrays...) //copy the given bitarrays.
	neighbor[beta] = neighbor[beta].Flip(uint(bit))
	return &neighbor
}

func genRandomBits(betaRange int64, numBetas int) *[]bits.BitArray {
	var bitarrays = make([]bits.BitArray, numBetas)
	maxBits := maxBits(betaRange)
	for i := 0; i < numBetas; i++ {
		bits := bits.NewBitArray64(maxBits, uint64(rand.Int63n(betaRange)))
		bitarrays[i] = bits
	}
	return &bitarrays
}

func maxBits(betaRange int64) int {
	return mathbits.Len64(uint64(betaRange))
}

func fitness(X, Y, b *mat.Dense) float64 {
	return regression.MSE(X, Y, b)
}

func bitsToMatrix(bits []bits.BitArray, betaRange float64) *mat.Dense {
	var betas = make([]float64, len(bits))
	for i, data := range bits {
		num := float64(data.Value().(uint64))
		betas[i] = num - (betaRange / 2)
	}
	return mat.NewDense(len(bits), 1, betas)
}

// Params contains the MaxGeneration and MaxRestart exit paramaters
type Params struct {
	MaxGeneration int
	MaxRestart    int
	XFile         string
	YFile         string
	BetaRange     int
}

func parseExitFile(exitFile *string) Params {
	dat, err := ioutil.ReadFile(*exitFile)
	check(err)
	var params Params
	err = json.Unmarshal(dat, &params)
	check(err)
	return params
}
