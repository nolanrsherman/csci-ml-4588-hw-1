// Author: Nolan Sherman - nolanrsherman@gmail.com
package main

import (
	"flag"
	"log"
	"strings"
)

func initFlags() *string {
	var exitFile = flag.String("exitFile", "", "The file that defines the programs exit conditions")
	flag.Parse()

	var err []string

	if *exitFile == "" {
		err = append(err, "--exitFile flag required")
	}

	if len(err) > 0 {
		flags := strings.Join(err, "\n")
		log.Fatal("Missing required flags:\n" + flags)
	}

	return exitFile
}
