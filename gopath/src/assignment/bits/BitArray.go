//Author: Nolan Sherman

//Package bits provides structures and functionality for manipulating long collection of bits.
package bits

// BitArray is an interface that provides methods for manipulating
// an array of bits
type BitArray interface {
	Set(uint) BitArray
	Flip(...uint) BitArray
	String() string
	Value() interface{}
}
