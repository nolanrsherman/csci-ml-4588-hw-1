//Author: Nolan Sherman

package bits

import (
	"fmt"
	"math/bits"
)

// NewBitArray64 returns a BitArray with the BitArrayInt64 Implementation
func NewBitArray64(capacity int, value uint64) BitArray {
	if capacity > 64 || capacity < 1 {
		panic("BitArray64 must have a capcity between 1 and 64 inclusively")
	}
	length := bits.Len64(value)
	if length > capacity {
		panic("Value provided that exceedes the BitArray's given capacity.")
	}
	return BitArrayInt64{value, uint(capacity)}
}

// BitArrayInt64 implements bit array with the underlying data
// structure of uint64. This means its max capacity is 64 bits.
type BitArrayInt64 struct {
	data     uint64
	capacity uint
}

// Set sets the bit at the position with the given value.
// Panics if the position is out of range. Returns the
// Resulting Bit Array
func (bits BitArrayInt64) Set(pos uint) BitArray {
	if pos < bits.capacity {
		bits.data = bits.data | (1 << pos)
		return bits
	}
	panic("Positon provided in Set() exceded the bounds of this BitArray")
}

// Unset unsets the bit at the position with the given value.
// Panics if the position is out of range. Returns the
// Resulting Bit Array
func (bits BitArrayInt64) Unset(pos uint) BitArray {
	if pos < bits.capacity {
		bits.data = bits.data &^ (1 << pos)
		return bits
	}
	panic("Positon provided in Unset() exceded the bounds of this BitArray")
}

//Flip changes bits to the opposite value for the specified positions
func (bits BitArrayInt64) Flip(positions ...uint) BitArray {
	var flipBits uint64
	for _, pos := range positions {
		if bits.capacity <= pos {
			panic("Position provided in Flip exceeded the bounds of BitArray")
		}
		flipBits = flipBits | (1 << pos)
	}
	bits.data = bits.data ^ flipBits
	return bits
}

// Value returns the value of this bit array. That is up to the nth bit
// where n is the capacity. This BitArrayInt64 bit array returns a uint64
// as interface{}.
func (bits BitArrayInt64) Value() interface{} {
	notmask := ^uint64(0) << uint(bits.capacity)
	return bits.data &^ notmask
}

// String provides the string representation of this BitArray
func (bits BitArrayInt64) String() string {
	str := fmt.Sprintf("%b", bits.data)
	return str[(64 - bits.capacity):]
}
