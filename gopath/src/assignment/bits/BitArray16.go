//Author: Nolan Sherman

package bits

import (
	"fmt"
	"math/bits"
)

// NewBitArray16 returns a BitArray with the BitArray16 Implementation
func NewBitArray16(capacity int, value uint16) BitArray {
	if capacity > 16 || capacity < 1 {
		panic("BitArray64 must have a capcity between 1 and 64 inclusively")
	}
	length := bits.Len16(value)
	if length > capacity {
		panic("Value provided that exceedes the BitArray's given capacity.")
	}
	return BitArray16{value, uint(capacity)}
}

// BitArray16 implements bit array with the underlying data
// structure of uint64. This means its max capacity is 64 bits.
type BitArray16 struct {
	data     uint16
	capacity uint
}

// Set sets the bit at the position with the given value.
// Panics if the position is out of range. Returns the
// Resulting Bit Array
func (bits BitArray16) Set(pos uint) BitArray {
	if pos < bits.capacity {
		bits.data = bits.data | (1 << pos)
		return bits
	}
	panic("Positon provided in Set() exceded the bounds of this BitArray")
}

// Unset unsets the bit at the position with the given value.
// Panics if the position is out of range. Returns the
// Resulting Bit Array
func (bits BitArray16) Unset(pos uint) BitArray {
	if pos < bits.capacity {
		bits.data = bits.data &^ (1 << pos)
		return bits
	}
	panic("Positon provided in Unset() exceded the bounds of this BitArray")
}

//Flip changes bits to the opposite value for the specified positions
func (bits BitArray16) Flip(positions ...uint) BitArray {
	var flipBits uint16
	for _, pos := range positions {
		if bits.capacity <= pos {
			panic("Position provided in Flip exceeded the bounds of BitArray")
		}
		flipBits = flipBits | (1 << pos)
	}
	bits.data = bits.data ^ flipBits
	return bits
}

// Value returns the value of this bit array. That is up to the nth bit
// where n is the capacity. This BitArray16 bit array returns a uint64
// as interface{}.
func (bits BitArray16) Value() interface{} {
	notmask := ^uint16(0) << uint(bits.capacity)
	return bits.data &^ notmask
}

// String provides the string representation of this BitArray
func (bits BitArray16) String() string {
	str := fmt.Sprintf("%b", bits.data)
	return str[(16 - bits.capacity):]
}
