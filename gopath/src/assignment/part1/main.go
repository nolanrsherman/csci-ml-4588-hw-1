// Author: Nolan Sherman - nolanrsherman@gmail.com
// CSCI4588 - Machine Learning II - Dr. Hoque
// Programming Assignment 1
// This is Part One, Calculating MSE

//NOT FINISHED: This part of the assignment was implemented
//with Octave. This program not guaranteed to run or output
//desired results.

package main

import (
	"assignment/input"
	"assignment/regression"
	"errors"
	"flag"
	"fmt"
	"log"

	"gonum.org/v1/gonum/mat"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func initFlags() (*string, *string) {
	var xFile = flag.String("xFile", "", "The file containing the matrix of input.")
	var yFile = flag.String("yFile", "", "The file containing the matrix of output.")
	flag.Parse()

	var err []error
	if *xFile == "" {
		err = append(err, errors.New("xFile flag required"))
	}

	if *yFile == "" {
		err = append(err, errors.New("yFile flag required"))
	}

	if len(err) > 0 {
		log.Fatal(err)
	}

	return xFile, yFile
}
func main() {
	xFile, yFile := initFlags()

	X, err := input.LoadMatrix(*xFile) //X input
	Y, err := input.LoadMatrix(*yFile) //Y output
	check(err)

	b := regression.FindBeta(X, Y)
	printMatrix("betas", b)
}

func printMatrix(label string, m *mat.Dense) {
	formatted := mat.Formatted(m, mat.Prefix(""), mat.Squeeze())
	fmt.Printf(label+" = \n%v\n\n", formatted)
}
