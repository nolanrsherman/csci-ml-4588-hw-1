//Author: Nolan Sherman

package input

import "gonum.org/v1/gonum/mat"

// LoadMatrix returns a matrix parsed from a given filepath. File is
// Parsed using Input.DefaultDelimeter which is '\t' by default. Returns
// a non-nill error if something went wrong.
func LoadMatrix(filepath string) (*mat.Dense, error) {
	//read csv file
	data, columns, rows, err := ReadCsv(DefaultDelimeter, filepath)
	if err != nil {
		return nil, err
	}

	matrix := mat.NewDense(rows, columns, data)
	return matrix, err
}
