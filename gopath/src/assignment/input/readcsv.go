//Author: Nolan Sherman

package input

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

// ReadCsv reads a seprated value file row by row and returns a float64 slice
// along with width, and height. Returns (data, width, height, error).
// Delimeter specifies the delimeter to be used. An error is returned
// if unsucessfully parsed. Expects values to be compatable with float64.
func ReadCsv(delimeter string, filepath string) ([]float64, int, int, error) {
	var data []float64
	width := 0
	height := 0
	//Read File
	file, err := os.Open(filepath)
	if err != nil {
		return data, width, height, err
	}
	defer file.Close()

	//Parse File
	scanner := bufio.NewScanner(file)
	for scanner.Scan() { //for each line
		height++
		line := scanner.Text()
		tokens := strings.Split(line, delimeter)
		if len(tokens) > width {
			width = len(tokens)
		}
		for _, num := range tokens {
			value, err := strconv.ParseFloat(num, 64)
			if err != nil { //return float parse error
				return data, width, height, err
			}
			data = append(data, value)
		}
	}

	//if scanner errored return
	err = scanner.Err()

	return data, width, height, err
}
