// Author: Nolan Sherman - nolanrsherman@gmail.com

//Package main is an executable package that computates the betas
//to maximize F (defined in f.go) non-deterministicly using
//a Simple Genetic Algorithm
package main

import (
	"fmt"
	"math/rand"
	"sort"
	"strings"
)

//NumBetas is the number of betas (variables) we are using
const NumBetas = 10

//MaxBitLength specifies the bit range to use in variables
//this will limit our betas to -1024 < x < 1023
const MaxBitLength = 11

func main() {
	params := initParams() //initializes params from passed json file.

	iterations := make([][]*Population, 0, params.MaxIterations)
	for i := 0; i < params.MaxIterations; i++ {
		generations := make([]*Population, 0, params.MaxGenerations)
		population := initialze(params.PopulationSize)
		sort.Sort(population)
		generations = append(generations, population)

		for j := 0; j < params.MaxGenerations; j++ {
			generation := evolve(population, params.EliteRate, params.CrossoverRate, params.MutationRate, params.PopulationSize)
			sort.Sort(generation)
			generations = append(generations, generation)
			population = generation
		}
		iterations = append(iterations, generations)
	}

	printResult(iterations)
}

//printResult prints the results given a [][]*Population
func printResult(iterations [][]*Population) {
	var sb strings.Builder
	t1 := "\n\n------ Iteration %v --------"
	t2 := "\n# Generation = %v, Fitness = %v"
	t3 := "\n////////////////////////////"
	for i, generations := range iterations {
		sb.WriteString(fmt.Sprintf(t1, i))
		for j, population := range generations {
			fitness := population.genes[0].Fitness
			sb.WriteString(fmt.Sprintf(t2, j, fitness))
		}
		sb.WriteString(t3)
	}
	fmt.Println(sb.String())
}

func evolve(population *Population, eliteRate, crossoverRate, mutationRate float64, nextSize int) *Population {
	elite, other := population.Harvest(eliteRate)
	other.CrossOver(crossoverRate)
	other.Mutate(mutationRate)
	randgenes := randomGenes(nextSize - elite.Len() - other.Len())
	newPop := NewPopulation(randgenes, fitness)
	newPop.Add(elite, other)
	return newPop
}

//initialze creates the initial population of Genes
//Recieves: size int => The size of the population
func initialze(size int) *Population {
	genes := randomGenes(size)
	return NewPopulation(genes, fitness)
}

//randomGenes creates a list of randomly generated genes
//of the given size.
func randomGenes(size int) []Gene16 {
	population := make([]Gene16, 0, size)
	maxInt := (1 << MaxBitLength) - 1 //the biggest int that can be made with bit length of MaxBitLength
	for i := 0; i < size; i++ {
		gene := NewGene16(MaxBitLength, NumBetas, randomBetas(NumBetas, maxInt)...)
		population = append(population, gene)
	}
	return population
}

//randomBetas returns a slice of int32 for random integers
//with given length and a maximuum value of max
func randomBetas(size, max int) []uint16 {
	values := make([]uint16, 0, size)
	for i := 0; i < size; i++ {
		rand := rand.Int31n(int32(max))
		values = append(values, uint16(rand))
	}
	return values
}

//fitness computes the fitness of a given
//gene in relation to F()
func fitness(gene *Gene16) float64 {
	rawBetas := gene.ValuesFloat64()
	var betas [10]float64
	copy(betas[:], rawBetas[:10])
	return F(betas)
}
