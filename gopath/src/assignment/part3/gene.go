// Author: Nolan Sherman - nolanrsherman@gmail.com

package main

import (
	"assignment/bits"
	"math/rand"
)

const maxCrossover = 50

//Gene16 represents a gene for our Genetic Algorithm
type Gene16 struct {
	Bits    []bits.BitArray
	Length  int
	MaxBits int
	Fitness float64
}

//NewGene16 returns a new gene that uses 16bit variable blocks.
//Recieves: maxBits int => The max number of bits to use of the 16 available.
//length => How many blocks this Gene has.
// values ... => list of values for the gene.
func NewGene16(maxBits, length int, values ...uint16) Gene16 {
	bitarr := make([]bits.BitArray, 0, length)
	gene := Gene16{bitarr, length, maxBits, 0}
	if len(values) > length {
		panic("Slice of values passed to NewGene16() has greater length than given value.")
	}
	for _, v := range values {
		bits := bits.NewBitArray16(maxBits, v)
		gene.Bits = append(gene.Bits, bits)
	}

	return gene
}

//ValuesFloat64 returns the list of betas this gene
//represents in a slice []float64
func (gene *Gene16) ValuesFloat64() []float64 {
	data := gene.Bits
	values := make([]float64, 0, gene.Length)
	offset := int16(1 << uint(gene.MaxBits-1))
	for _, bits := range data {
		v := int16(bits.Value().(uint16)) - offset
		values = append(values, float64(v))
	}

	return values
}

//Replicate returns an exact deep copy of this Gene, not a reference
//to original underlying data structures.
func (gene *Gene16) Replicate() Gene16 {
	var bits = append([]bits.BitArray(nil), gene.Bits...) //copy the given bitarrays.
	return Gene16{bits, gene.Length, gene.MaxBits, gene.Fitness}
}

//Cross performs as random genetic cross operation between
//this gene and a given mate. It returns the two resulting
//genes.
func (gene *Gene16) Cross(mate *Gene16) (Gene16, Gene16) {

	setA := gene.Replicate()
	setB := mate.Replicate()
	pA := rand.Intn(len(setA.Bits)) //positionA
	pB := rand.Intn(len(setB.Bits)) //positionB
	A := setA.Bits[pA]
	setA.Bits[pA] = setB.Bits[pB]
	setB.Bits[pB] = A

	return setA, setB
}
