// Author: Nolan Sherman - nolanrsherman@gmail.com
package main

import "log"

//Check recieves and error and then
//logs and exits fatally if it is not nill.
func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
