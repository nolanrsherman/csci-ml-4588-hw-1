// Author: Nolan Sherman - nolanrsherman@gmail.com
package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"strings"
)

// Params represents the paramaters passed to this program
type Params struct {
	PopulationSize int
	CrossoverRate  float64
	MutationRate   float64
	EliteRate      float64
	MaxGenerations int
	MaxIterations  int
}

// initParams initializes the flag parser for --params and processes
// given file and then returns the Params struct containing given values
func initParams() Params {

	var paramFile = flag.String("params", "", "The json file containing options for this program.")
	flag.Parse()

	var errors []string
	if *paramFile == "" {
		errors = append(errors, "--params flag required")
	}

	if len(errors) > 0 {
		flags := strings.Join(errors, "\n")
		log.Fatal("Missing required flags:\n" + flags)
	}

	dat, err := ioutil.ReadFile(*paramFile)
	check(err)
	var params Params
	err = json.Unmarshal(dat, &params)
	check(err)
	return params

}
