// Author: Nolan Sherman - nolanrsherman@gmail.com
package main

import "math"

//F is the function this program attempts to optimize
var F = func(betas [10]float64) float64 {

	//Calculating Part A
	a := float64(1) // 1

	//Calculating part B
	b := float64(0)
	for i := 0; i < len(betas); i++ {
		x := betas[i]
		b = b + (x * x / 4000)
	}

	//Calculating part C
	cx := func(i int, x float64) float64 {
		return math.Cos(x / math.Sqrt(float64(i)))
	}

	c := cx(1, betas[0])
	for i := 2; i < len(betas); i++ {
		c = c * cx(i, betas[i-1])
	}

	//Return the value for function
	return a + b - c

}
