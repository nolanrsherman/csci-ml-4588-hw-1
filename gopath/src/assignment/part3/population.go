// Author: Nolan Sherman - nolanrsherman@gmail.com
package main

import (
	"math/rand"
)

//Population is a collection of Genes
//implements sort.interface
type Population struct {
	genes   []Gene16
	fitness func(*Gene16) float64
}

//NewPopulation returns a new poulation with the given genes.
func NewPopulation(genes []Gene16, fitness func(*Gene16) float64) *Population {
	pop := &Population{genes, fitness}
	pop.calcFitness()
	return pop
}

//calcFitness calculates the fitness of each gene in
//the population with the given fitness function
func (pop *Population) calcFitness() {
	for i := 0; i < len(pop.genes); i++ {
		pop.genes[i].Fitness = pop.fitness(&pop.genes[i])
	}
}

// Len is the number of elements in the collection.
func (pop *Population) Len() int {
	return len(pop.genes)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (pop *Population) Less(i, j int) bool {
	return pop.genes[i].Fitness > pop.genes[j].Fitness
}

// Swap swaps the elements with indexes i and j.
func (pop *Population) Swap(i, j int) {
	a := pop.genes[i]
	pop.genes[i] = pop.genes[j]
	pop.genes[j] = a
}

//CloneAll returns the reference to a deep copy of
//the original population
func (pop *Population) CloneAll() *Population {
	return pop.cloneRange(0, pop.Len())
}

//Harvest returns two []Gene based on the specified ratios. Assumes Population
//has already been sorted based on fitness.
func (pop *Population) Harvest(eliteRatio float64) (*Population, *Population) {
	numElite := int(eliteRatio * float64(pop.Len()))
	elite := pop.cloneRange(0, numElite)
	cross := pop.cloneRange(0, pop.Len())
	return elite, cross
}

//cloneRange returns a deep copy of the population from a given range
func (pop *Population) cloneRange(start, end int) *Population {
	genes := make([]Gene16, 0, end-start)
	for _, gene := range pop.genes[start:end] {
		genes = append(genes, gene.Replicate())
	}
	return &Population{genes, pop.fitness}
}

//Add adds the given populations to this population.
func (pop *Population) Add(populations ...*Population) {
	for _, newPop := range populations {
		pop.genes = append(pop.genes, newPop.genes...)
	}
}

//CrossOver runs a cross over algorithm on a given
//ratio of this population. Assumes population is sorted.
//The current state of this population is overwritten.
func (pop *Population) CrossOver(ratio float64) {
	numPairs := int(ratio * float64(pop.Len()) / 2)

	genes := make([]Gene16, 0, numPairs*2)
	sumFitness := pop.sumFitness()
	for i := 0; i < numPairs; i++ {
		a := pop.rouletteSelect(sumFitness)
		b := pop.rouletteSelect(sumFitness)

		//perform cross
		ab, ba := a.Cross(&b)
		genes = append(genes, ab, ba)
	}
	pop.genes = genes //switchout for the new genes
	pop.calcFitness() //re calculate fitness
}

func (pop *Population) sumFitness() float64 {
	var sum float64
	for _, gene := range pop.genes {
		sum = sum + gene.Fitness
	}
	return sum
}

//rouletteSelect selects a gene using roulette algorithm. Assumes population
//is sorted by fitness.
func (pop *Population) rouletteSelect(fitnessTotal float64) Gene16 {
	r := rand.Float64()
	pF := float64(1)
	for _, gene := range pop.genes {
		p := gene.Fitness / fitnessTotal //probability of acceptance
		offset := pF - p
		if r < pF && r > offset {
			return gene
		}
		pF = offset
	}
	panic("rouletteSelect could not make a selection.")
}

//Mutate randomly mutates genes in this population
//where the number of genes is determined by mutationRate
func (pop *Population) Mutate(mutationRate float64) {
	num := int(float64(pop.Len()) * mutationRate)
	for i := 0; i < num; i++ {
		j := rand.Intn(pop.Len())
		l := rand.Intn(len(pop.genes[0].Bits))
		b := rand.Intn(pop.genes[l].MaxBits)
		pop.genes[j].Bits[l].Flip(uint(b))
	}
}
